//
//  CasesCollectionViewCell.swift
//  CaseSimulator
//
//  Created by kira on 04.12.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import UIKit

final class CasesCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOUtlets
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var gunName: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var textureImage: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()
        image.image = nil
        //bottomView.backgroundColor = .white
        gunName.text = ""
        priceLabel.text = ""
    }
}

// MARK: - Config
extension CasesCollectionViewCell {

    func config(object: GunModelObject) {
        backgroundColor = .white
        if App.languageString == "ru" {
            gunName.text = object.name_rus
        } else {
            gunName.text = object.name
        }

        textureImage.image = object.rurityEnum.texture

        priceLabel.layer.cornerRadius = 5
        priceLabel.clipsToBounds = true
        priceLabel.text =  String(format: "%.0f$", object.price)
        //bottomView.backgroundColor = RuritySkin(rawValue: object.rurity)?.color
        image.image = UIImage(named: object.image)
    }
}
