//
//  WinGunView.swift
//  CaseSimulator
//
//  Created by kira on 2/20/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit

final class WinGunView: UIView {

    //MARK: - IBOutlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var gunImage: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commomInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commomInit()
    }

    private func commomInit() {
        Bundle.main.loadNibNamed(R.nib.winGunView.name, owner: self, options: nil)
        addSubviewWithDefaultConstraints(view)
    }

    func config(data: GunModelObject) {
        topView.backgroundColor = data.rurityEnum.color
        bottomView.backgroundColor = data.rurityEnum.color
        priceLabel.text = String(format: "%.0f", data.price) + "$"

        titleLabel.text = R.string.localization.newItemObtained().capitalized + "!"

        if App.languageString == "ru" {
            nameLabel.text = data.name_rus
        } else {
            nameLabel.text = data.name
        }

        gunImage.image = UIImage(named: data.image) ?? UIImage()
        rounded(radius: 5)

    }

}
