//
//  WeaponsView.swift
//  CaseSimulator
//
//  Created by kira on 6/10/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

final class WeaponsView: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet var backView: UIView!
    @IBOutlet var nameLabel: UILabel!

    var index: Int!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commomInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commomInit()
    }

    private func commomInit() {
        Bundle.main.loadNibNamed("WeaponsView", owner: self, options: nil)
        addSubviewWithDefaultConstraints(view)
    }

    func config(data: GunModelObject, index: Int) {
        image.image = UIImage(named: data.image)
        backView.backgroundColor = .black//data.rurityEnum.color
        self.view.backgroundColor = .black//data.rurityEnum.color
        if App.languageString == "ru" {
            nameLabel.text = data.name_rus
        } else {
            nameLabel.text = data.name
        }

        self.index = index

        layer.cornerRadius = 10
        clipsToBounds = true
    }

}
