//
//  DrawOfWapons.swift
//  CaseSimulator
//
//  Created by kira on 2/6/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Crashlytics
import SnapKit

final class DrawOfWapons: UIView {

    // MARK: - IBOutlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var strokeView: UIView!
    @IBOutlet weak var winViewBottomConstrait: NSLayoutConstraint!
    @IBOutlet weak var winVewHeaight: NSLayoutConstraint!
    @IBOutlet weak var winView: WinGunView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var scrollingLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stoneTextureImage: UIImageView!

    // MARK: - Properties
    private var interval: Double = 0.05
    private var realmProvider = RealmProvider()

    private var amount = 0
    private var isFirstOpen = true
    private var isScrolling = true {
        didSet {
            changeConfirmButton()
        }
    }

    private var winnedExp = 0
    private var winnedMoney: Double = 0
    private var winnedRurity: RuritySkin = .rare

    private var currentIndex = IndexPath(row: 0, section: 0)
    private var timer = Timer()

    private var winnedGun: WinnedGunModelObject?
    private var winedRurityCase: RurityCase?

    private let audioManager = AudioManager()

    private var firsttModel = [GunModelObject]() {
        didSet {
            modelForGift = modelForGift(model: firsttModel)
        }
    }

    private var modelForGift = [GunModelObject]() {
        didSet {
            createViews()
        }
    }

    private var modelViews   = [WeaponsView]()
    private var modelForLeaderboatd = [WinnedGunModelObject]()

    weak var delegate: CasesViewControllerProtocol?
    private let nameView = "DrawOfWapons"

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commomInit()
    }

    init(model: [GunModelObject]) {
        super.init(frame: .zero)
        self.commomInit()
    }

    func setModel(model: [GunModelObject]) {
        firsttModel = model
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commomInit()
    }

    //need some help
    override func draw(_ rect: CGRect) {

        fillScrollView()

        confirmButton.layer.cornerRadius = 5
        stoneTextureImage.layer.cornerRadius = 5
        confirmButton.clipsToBounds = true
        stoneTextureImage.clipsToBounds = true

        confirmButton.isUserInteractionEnabled = false

        confirmButton.isHidden = true
        stoneTextureImage.isHidden = true

        confirmButton.titleLabel?.text = R.string.localization.confirm().uppercased()
        scrollingLabel.text = R.string.localization.scrolling() + "..."

        if isFirstOpen == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                self?.audioManager.play(sound: .cocking)
        }
            //self.timerMethod()
            //setViesInToScrollView()
        } else {
            removeFromSuperview()
        }

        Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
    }

    private func clearAll() {
        for item in scrollView.subviews {
            item.removeFromSuperview()
        }
        self.scrollView.contentOffset.x = 0
    }

    private func setFirstView(firstView: UIView) {
        scrollView.addSubview(firstView)
        firstView.snp.makeConstraints { (make) in
            make.height.equalTo(scrollView)
            make.width.equalTo(scrollView.bounds.width / 3)
            make.left.equalTo(0)
            make.centerY.equalTo(scrollView)
        }
    }

    private func fillNewView(newView: UIView, preView: UIView) {
        scrollView.addSubview(newView)
        newView.snp.makeConstraints { (make) in
            make.height.equalTo(preView)
            make.width.equalTo(preView)
            make.centerY.equalTo(preView)
            make.left.equalTo(preView.snp.right)
        }
    }

    private func estimateWidth() -> CGFloat {
        let width = scrollView.bounds.width
        return CGFloat(modelForGift.count) * (CGFloat(width) / CGFloat(3))
    }

    private func fillScrollView() {

        guard let firstView = modelViews.first else { return }
        setFirstView(firstView: firstView)

        for (index, item) in modelViews.enumerated() {
            if index >= 1 {
                fillNewView(newView: item, preView: modelViews[index - 1])
            }
        }
        start()
    }

    private func createViews() {
        for (index, item) in modelForGift.enumerated() {
            let newView = WeaponsView(frame: .zero)
            newView.config(data: item, index: index)
            modelViews.append(newView)
        }
    }

    func start() {
        animate { (winned) in
            self.audioManager.play(sound: .winSound)
            let winnedIndex = winned.index
            let winnedGun   = self.modelForGift[winnedIndex!]
            self.winnedGun  = WinnedGunModelObject(gun: winnedGun)
            self.winnedExp  = winnedGun.rurityEnum.experience
            self.winedRurityCase = winnedGun.ownerCase?.rurityEnum
            Defaults.experions.set(Defaults.experions.intValue + self.winnedExp)
            self.winnedMoney = Double(winnedGun.price)
            self.winView.config(data: winnedGun)
            self.confirmButton.isUserInteractionEnabled = true
            self.winViewBottomConstrait.constant += self.winVewHeaight.constant + 15
            UIView.animate(withDuration: 1, animations: {
                self.layoutIfNeeded()
                self.changeConfirmButton()
            })
        }
    }

    func animate(completion: @escaping(_ isWin: WeaponsView) -> Void) {

        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 6, delay: 0, options: .curveEaseInOut, animations: {
            self.scrollView.contentOffset.x = self.estimateWidth() - self.scrollView.bounds.size.width - self.scrollView.bounds.size.width / 4
        }) { (position) in
            completion(self.computedWinner())
        }
    }

    private func computedWinner() -> WeaponsView {
        for view in scrollView.subviews {
            let rectOfViewnSuperview = self.scrollView.convert(view.frame, to: self.view)
            if rectOfViewnSuperview.intersects(self.strokeView.frame) {
                if let view = view as? WeaponsView {
                    return view
                }
            }
        }
        return WeaponsView(frame: .zero)
    }

    private func commomInit() {
        Bundle.main.loadNibNamed(R.nib.drawOfWapons.name, owner: self, options: nil)
        addSubviewWithDefaultConstraints(view)
    }
}

// MARK: - Timer
extension DrawOfWapons {

    @objc private func timerMethod() {
        isFirstOpen = false
        let animation = UIViewPropertyAnimator(duration: interval, curve: .linear) {
            self.layoutIfNeeded()
        }

        animation.addCompletion { position in
            self.amount += 1
            self.interval += 0.01
            //self.audioManager.play(sound: .roll)

            if self.amount == self.modelForGift.count - self.firsttModel.count {
                self.isScrolling = false

                   self.audioManager.play(sound: .shoot)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                    self?.audioManager.play(sound: .winSound)
                }



//                if let winedGun = self.computedWinGun() {
//                    self.winedRurityCase = winedGun.ownerCase?.rurityEnum
//                    self.winnedGun = WinnedGunModelObject(gun: winedGun)
//
//                    guard let winnedGun = self.winnedGun else {
//                        self.layoutIfNeeded()
//                        return
//                    }
//
//                    self.winnedExp = winnedGun.rurityEnum.experience
//                    Defaults.experions.set(Defaults.experions.intValue + self.winnedExp)
//                    self.winnedMoney = Double(winnedGun.price)
//                    self.winnedRurity = winnedGun.rurityEnum
//                    self.winView.config(data: winedGun)
//                    self.confirmButton.isUserInteractionEnabled = true
//
//                    self.winViewBottomConstrait.constant += self.winVewHeaight.constant + 15
//                    UIView.animate(withDuration: 1, animations: {
//                        self.layoutIfNeeded()
//                    })
//                } else {
//                    self.removeFromSuperview()
//                }
            } else {
                let newRow = self.currentIndex.row + 1
                self.currentIndex = IndexPath(row: newRow, section: 0)
                if self.amount < self.modelForGift.count {
                    self.timerMethod()
                }
            }
        }
        animation.startAnimation()
    }

    private func saveWinnedRyrity() {
        switch winnedRurity {
        case .rare:
            Defaults.rare.set(Defaults.rare.intValue + 1)
        case .mythical:
            Defaults.mythical.set(Defaults.mythical.intValue + 1)
        case .legendary:
            Defaults.legendary.set(Defaults.legendary.intValue + 1)
        case .immortal:
            Defaults.immortal.set(Defaults.immortal.intValue + 1)
        case .arcana:
            Defaults.arcana.set(Defaults.arcana.intValue + 1)
        case .ancient:
            Defaults.arcana.set(Defaults.ancient.intValue + 1)
        }
    }

    private func changeConfirmButton() {
        scrollingLabel.isHidden = true
        confirmButton.isHidden = false
        stoneTextureImage.isHidden = false
    }

}

// MARK: - Data act's
extension DrawOfWapons {

    //возвращает ружья, преумноженые и потасованые
    private func modelForGift(model: [GunModelObject]) -> [GunModelObject] {
        var giftsArray = [GunModelObject]()
        model.forEach {
            if let gun = $0.copy() as? GunModelObject {
                //предотвращаем слишком быстрый скрол
                //при большом количестве айтемов в кейсе
                if model.count > 30 {
                    giftsArray += [gun]
                } else {
                    giftsArray += itemsByRurity(item: gun)
                }
            }
        }
        return giftsArray.shuffled()
    }

    //возвращает оружия в количестве зависящего от уникальности ружья
    private func itemsByRurity(item: GunModelObject) -> [GunModelObject] {
        var guns = [GunModelObject]()
        guns = Array(repeating: item, count: item.rurityEnum.increase)
        return guns
    }
}

//MARK: - Actions
extension DrawOfWapons {
    @IBAction func confirmButtonTap(_ sender: UIButton) {
        audioManager.stop()
        updateStats()
        saveWinnedRyrity()
        audioManager.play(sound: .click)
        delegate?.changeUserEsp(from: Defaults.experions.intValue - winnedExp,
                                to: Defaults.experions.intValue, winnedCaseRurity: winedRurityCase)

        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            self.removeFromSuperview()
        }

    }

    private func updateStats() {
        Defaults.gained.set(Defaults.gained.floatValue + self.winnedMoney)
        Defaults.amountOpenCases.set(Defaults.amountOpenCases.intValue + 1)
        guard let winnedGun = self.winnedGun else { return }
        self.realmProvider.create(object: winnedGun)
        App.updateValue(newValue: realmProvider.totalPriceWinnedGun())
    }
}
