//
//  PopupCaseView.swift
//  CaseSimulator
//
//  Created by kira on 09.12.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import UIKit
import AVFoundation
import Crashlytics

final class PopupCaseView: UIViewController {
    typealias Model = PopupCaseView

// MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!

// MARK: - Properties
    private let amountColums = 4
    private var minimumSpacing: CGFloat {
        return collectionView.flowLayout?.minimumInteritemSpacing ?? CGFloat(5)
    }

    var model: PopupCaseViewModelProtocol = PopupCaseViewModel()
    //weak var delegate: CaseViewActionDelegateProtocol?
    private var player: AVAudioPlayer?
    private let nameView = "PopupCaseView"
    weak var delegate: CustomtabBarControllerAdvertising?

    static func initFromStoryboard(storyboardName: String) -> Model? {
        if let controller = UIStoryboard(name: "PopupCaseViewController", bundle: nil).instantiateInitialViewController() as? Model {
            return controller
        }
        return nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(R.nib.caseCollectionViewCell)

        collectionView.collectionViewLayout = PixelArtCollectionViewFlowLayout()

        setConstraints()
        NotificationCenter.default.addObserver(forName: Notification.Name("caseOpened"), object: nil, queue: nil) { (notification) in
            self.collectionView.reloadData()
        }

        let nib = UINib(nibName: R.nib.collectionReusableViewT.name, bundle: nil)

        collectionView.register(nib,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: R.nib.collectionReusableViewT.identifier)

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
        view.layer.cornerRadius = 5

        Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
    }

    func setConstraints() {
        //self.view.translatesAutoresizingMaskIntoConstraints = false
        let minimum: CGFloat = (3 * 5)
        let tmp: CGFloat =  (self.view.bounds.width / CGFloat(amountColums))

        //((self.bounds.width / CGFloat(amountColums)) - (3 * 5)) * 6 + (6 * 5)

        //caseCollctionViewHeightConstraint.constant = (tmp - minimum) * 6 + minimum
        view.layoutIfNeeded()
    }
}

// MARK: - Methods
extension PopupCaseView {

    private func caseViewHeightConstraint() -> CGFloat {
        var height: CGFloat = 0

        //height = (collectionView.bounds.width / CGFloat(amountColums)) - ((amountColums - 1) * minimumSpacing) * 6 + (6 * minimumSpacing)

        return height
    }
}

// MARK: - UICollectionVieDelegate
extension PopupCaseView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if model.item(by: indexPath).rurityEnum != .branded_cases {
            let defaults = UserDefaults.standard.string(forKey: model.item(by: indexPath).uuId)
            if defaults != model.item(by: indexPath).uuId {
                delegate?.presentAlertViewController(item: model.item(by: indexPath))
            } else {
                delegate?.showCaseDetails(didTapItem: model.item(by: indexPath))
            }
        } else {
            delegate?.showCaseDetails(didTapItem: model.item(by: indexPath))
        }
    }
}

// MARK: - UICollectionViewDataSource
extension PopupCaseView: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                                   withReuseIdentifier: R.nib.collectionReusableViewT.identifier, for: indexPath) as? CollectionReusableViewT {
                sectionHeader.textLabel.text = RurityCase.sectionString(section: indexPath.section).uppercased()
                return sectionHeader
            }
            return UICollectionReusableView()
        }
        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        var height: CGFloat = 0
        if UIDevice.current.userInterfaceIdiom == .pad {
            height = 75
        } else {
            height = 50
        }
        return CGSize(width: collectionView.frame.width, height: height)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.amoutItemsIn(section: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.caseCollectionViewCell.identifier, for: indexPath) as! CaseCollectionViewCell

        cell.config(model: model.item(by: indexPath))
        return cell
    }
}

// MARK: - Actions
extension PopupCaseView {
    @IBAction func closeButtonTap(sender: UIButton) {
        playSound(soundName: "closeSound.mp3")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.dismiss(animated: true, completion: nil)
        }
    }

    private func didTapCase(indexPath: IndexPath) {
        playSound(soundName: "click.mp3")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.13) {
            self.dismiss(animated: true, completion: nil)
        }
    }

    private func playSound(soundName: String) {
        guard let path = Bundle.main.path(forResource: soundName, ofType: nil) else { return }
        let url = URL(fileURLWithPath: path)
        do {
            player = try AVAudioPlayer(contentsOf: url)
            player?.play()
        } catch {
            print("Can't load mp3 file")
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PopupCaseView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width / CGFloat(amountColums)) - minimumSpacing
        return CGSize(width: width, height: width)
    }
}

// MARK: - PixelArtCollectionViewFlowLayout
final class PixelArtCollectionViewFlowLayout: UICollectionViewFlowLayout {

    // MARK: - Lifecycle
    override init() {
        super.init()
        setupLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }

// MARK: - Private
    private func setupLayout() {
        minimumInteritemSpacing = 5
        minimumLineSpacing = 5
        sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
