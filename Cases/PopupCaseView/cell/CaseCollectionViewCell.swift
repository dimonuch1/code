//
//  CaseCollectionViewCell.swift
//  CaseSimulator
//
//  Created by kira on 09.12.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import UIKit

final class CaseCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var lvlRestriction: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

//MARK: - Config
extension CaseCollectionViewCell {

    func config(model: CaseModelObject) {

        let defaults = UserDefaults.standard.string(forKey: model.uuId)

        switch model.rurityEnum {
        case .limited_edition:
            if Defaults.experions.intValue < RankEnum.guardian.neededExp {
                lvlRestriction.image = R.image.silver1()
                lvlRestriction.isHidden  = false
                //isUserInteractionEnabled = false
            } else if defaults != model.uuId {
                //set image video
                isUserInteractionEnabled = true
                lvlRestriction.isHidden  = false
                lvlRestriction.image = R.image.video()
            } else {
                isUserInteractionEnabled = true
                lvlRestriction.isHidden  = true
            }
        case .rarity_cases:

            if Defaults.experions.intValue < RankEnum.crusader.neededExp {
                lvlRestriction.image = R.image.silver2()
                lvlRestriction.isHidden  = false
                //isUserInteractionEnabled = false
            } else if defaults != model.uuId {
                //set image video
                //isUserInteractionEnabled = true
                lvlRestriction.isHidden  = false
                lvlRestriction.image = R.image.video()
            } else {
                isUserInteractionEnabled = true
                lvlRestriction.isHidden  = true
            }
        case .branded_cases:
            //delegate to custom tab bar
            //let defaults = UserDefaults.standard.string(forKey: model.uuId)
            //if Defaults.experions.intValue < RankEnum.archon.neededExp {
                //set image runk
//                lvlRestriction.isHidden = false
//                lvlRestriction.image = R.image.silver4()
//                isUserInteractionEnabled = false
//            } else  if defaults != model.uuId {
//                //set image video
//                lvlRestriction.isHidden = false
//                lvlRestriction.image = R.image.video()
            //} else {
            isUserInteractionEnabled    = true
                lvlRestriction.isHidden = true
            //}
        }

        if model.image == "" {
            mainImage.image = UIImage()
        } else { mainImage.image = UIImage(named: model.image) }

        if App.languageString == "ru" {
            titlelabel.text = model.name_rus
        } else {
            titlelabel.text = model.name
        }
    }
}
