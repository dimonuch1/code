//
//  CollectionReusableViewT.swift
//  CaseSimulator
//
//  Created by kira on 4/3/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

final class CollectionReusableViewT: UICollectionReusableView {

    @IBOutlet var textLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
