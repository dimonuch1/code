//
//  PopupCaseViewModel.swift
//  CaseSimulator
//
//  Created by kira on 1/18/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

protocol PopupCaseViewModelProtocol {
    func amountItems() -> Int
    func updateModel()
    func amoutItemsIn(section: Int) -> Int
    func item(by indexPath: IndexPath) -> CaseModelObject
}

final class PopupCaseViewModel {
    private var realmProvider = RealmProvider()
    private var model: [CaseModelObject] = []

    init() {
        updateModel()
    }
}

// MARK: - PopupCaseViewModelProtocol
extension PopupCaseViewModel: PopupCaseViewModelProtocol {

    func amountItems() -> Int {
        return model.count
    }

    func updateModel() {
        model = realmProvider.fetchAllByCase(with: nil, type: CaseModelObject.self) as! [CaseModelObject]
    }

    func amoutItemsIn(section: Int) -> Int {
        return model.filter({ $0.rurityEnum.spotInList == section }).count
    }

    func item(by indexPath: IndexPath) -> CaseModelObject {
        return model.filter({ $0.rurityEnum.spotInList == indexPath.section })[indexPath.row]
    }
}
