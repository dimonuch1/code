//
//  LeaderboardCongratsViewController.swift
//  CaseSimulator
//
//  Created by kira on 7/6/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

final class LeaderboardCongratsViewController: UIViewController {

    //typealias Model = LeaderboardCongratsViewController

    @IBOutlet var mainImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        mainImage.layer.cornerRadius = 30
        mainImage.clipsToBounds = true

        if App.languageString == "ru" {
            mainImage.image = UIImage(named: "leaderboardCongratsRus")
        } else {
            mainImage.image = UIImage(named: "leaderboardCongratsEng")
        }
        
    }

    @IBAction func close() {
        dismiss(animated: true) {
            Defaults.isShowJacpotCongrats.set(true)
        }
    }

}
