//
//  CasesViewModel.swift
//  CaseSimulator
//
//  Created by kira on 03.12.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import Foundation
import UIKit

protocol CasesViewModelProtocol: class {

    func amountElement() -> Int
    func item(at index: Int) -> GunModelObject
    func changeCaseOwner(newOwner: CaseModelObject)
    func guns() -> [GunModelObject]
    var delegate: PopupCaseViewProtocol? { get set }
    func caseImage() -> UIImage
    func keyImage() -> UIImage
    func caseName() -> String
    func isCaseOwner() -> Bool
}

class CasesViewModel {

    weak var delegate: PopupCaseViewProtocol?
    private var realmProvider = RealmProvider()
    private var model: [GunModelObject] = []
    private var caseOwner: CaseModelObject? {
        didSet {
            updateModel()
        }
    }

    init() {
        //updateModel()
    }
}

// MARK: - CasesViewModelProtocol
extension CasesViewModel: CasesViewModelProtocol {

    func guns() -> [GunModelObject] {
        return model
    }

    func changeCaseOwner(newOwner: CaseModelObject) {
        self.caseOwner = newOwner
        //updateModel()
    }

    func amountElement() -> Int {
        return model.count
    }

    func item(at index: Int) -> GunModelObject {
        return model[index]
    }

    private func updateModel() {
        if let caseOwner = self.caseOwner {
            let predicate = NSPredicate(format: "ownerCase == %@", caseOwner)
            model.removeAll()
            model = realmProvider.fetchAllByCase(with: predicate, type: GunModelObject.self) as! [GunModelObject]
            delegate?.modelDidUpdate()
        }
    }

    func caseImage() -> UIImage {
        if let caseOwner = self.caseOwner {
            return UIImage(named: caseOwner.image) ?? UIImage()
        } else {
            return UIImage()
        }
    }

    func keyImage() -> UIImage {
        if let caseOwner = self.caseOwner {
            return UIImage()
        } else {
            return UIImage()
        }
    }

    func caseName() -> String {
        if App.languageString == "ru" {
            return caseOwner?.name_rus ?? ""
        } else {
            return caseOwner?.name ?? ""
        }
    }

    func isCaseOwner() -> Bool {
        if caseOwner == nil {
            return false
        } else {
            return true
        }
    }
}
