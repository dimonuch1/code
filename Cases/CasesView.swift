//
//  CasesView.swift
//  CaseSimulator
//
//  Created by kira on 28.11.2018.
//  Copyright © 2018 Dmitry. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation
import Crashlytics

enum Tag: Int {
    case caseView = 2
}

protocol PopupCaseViewProtocol: class {
    func modelDidUpdate()
}

final class CasesViewController: UIViewController, InitFromStoryboard {
    typealias Model = CasesViewController

// MARK: - IOBoutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var caseImage: UIImageView!
    @IBOutlet weak var caseLabel: UILabel!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var openCaseButton: UIButton!

// MARK: - Properties
    weak var delegate: CustomtabBarControllerAdvertising?

    var model: CasesViewModelProtocol = CasesViewModel()
    var ownerCase: CaseModelObject!

    private let audioManager = AudioManager()

    private var isCaesShow = false
    private let nameView = "CasesViewController"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewDidApearConfig()
    }
}

// MARK: - Config
extension CasesViewController {

    private func viewDidApearConfig() {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"

        //imageView.dropShadow(width: 2, height: 2)

//        if isCaesShow, model.isCaseOwner() {
//            isCaesShow = false
//        }

        openCaseButton.dropShadow()
        openCaseButton.setTitle(R.string.localization.openCase(), for: .normal)

//        if !model.isCaseOwner() {
//            if !isCaesShow {
//                isCaesShow = true
//                if let lastDate = dateFormatter.date(from: "01.08.2019") {
//                    if !Defaults.isShowJacpotCongrats.boolValue, Date() < lastDate {
//                        delegate?.showLeaderboardCongrets()
//                    }
//                }
//            }
//        }
        Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
    }

    private func config() {
        model.delegate = self
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(R.nib.casesCollectionViewCell)
        model.changeCaseOwner(newOwner: ownerCase)

        openCaseButton.layer.cornerRadius = 5
        if let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView {
            statusBarView.backgroundColor = .clear
        }
    }
}

// MARK: - UICollectionViewDelegate
extension CasesViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //showItem(indexPath: indexPath)
    }

    private func showItem(indexPath: IndexPath) {
        audioManager.play(sound: .click)
        let itemView = ItemView()
        itemView.configFromCases(model: model.item(at: indexPath.row))
        view.addSubviewWithDefaultConstraints(itemView)
    }
}

// MARK: - UICollectionViewDataSource
extension CasesViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.amountElement()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.casesCollectionViewCell.identifier,
                                                            for: indexPath) as? CasesCollectionViewCell else {
                                                                return UICollectionViewCell()
        }

        cell.config(object: model.item(at: indexPath.row))
        cell.layer.cornerRadius = 5
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CasesViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.bounds.width / 4.2
        let height = self.collectionView.bounds.height / 4.2
                return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 3, bottom: 5, right: 3)
    }
}

// MARK: - Actions
extension CasesViewController {

    @IBAction func dismiss() {
        NotificationCenter.default.post(name: Notification.Name("caseOpened"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func openCaseDidTap(_ sender: UIButton) {
        if Defaults.amountOpenCases.intValue % App.interstitiialAfterOpenCases == 0,
            Defaults.amountOpenCases.intValue != 0 {
            delegate?.showAdvertisingFromCase()
            Defaults.amountOpenCases.set(Defaults.amountOpenCases.intValue + 1)
        } else {
            openCaseButton.isUserInteractionEnabled = false
            if !model.guns().isEmpty {
                audioManager.play(sound: .click)
                let gift = DrawOfWapons()
                gift.setModel(model: model.guns())
                gift.delegate = self
                view.addSubviewWithDefaultConstraints(gift)
            }
            openCaseButton.isUserInteractionEnabled = true
        }
    }

    private func animateCells() {
        let scaleAnimation = AnimationType.zoom(scale: 2)
        self.collectionView.animateAll(withType: [scaleAnimation])
    }
}

// MARK: - CaseViewActionDelegateProtocol
extension CasesViewController: CaseViewActionDelegateProtocol {
    func popupCaseView(_ popupCaseView: PopupCaseView, didSelectItem item: CaseModelObject) {
        model.changeCaseOwner(newOwner: item)
    }

    func popUpAlertController(item: CaseModelObject) {
        delegate?.presentAlertViewController(item: item)
    }
}

// MARK: - Model Protocol
// MARK: - PopupCaseViewProtocol
extension CasesViewController: PopupCaseViewProtocol {
    func modelDidUpdate() {
        collectionView.reloadData()

        caseImage.image       = model.caseImage()
        caseLabel.text        = model.caseName()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            self.animateCells()
        }
    }
}

// MARK: - CasesViewControllerProtocol
extension CasesViewController: CasesViewControllerProtocol {
    func changeUserEsp(from: Int, to: Int, winnedCaseRurity: RurityCase?) {
        if RankEnum.isChangRank(from: from, to: to) {
            delegate?.presentLevelUpViewControllrt()
        } else if winnedCaseRurity != .branded_cases {
            if Defaults.rateUs.intValue == 10 || Defaults.rateUs.intValue == 30 || Defaults.rateUs.intValue == 50 || Defaults.rateUs.intValue == 70 || Defaults.rateUs.intValue == 100 { //Defaults.amountRateUsShowed.intValue == 0 {
                //Defaults.amountRateUsShowed.set(1)
                AppStoreReviewManager.requestReviewIfAppropriate()
            }
            Defaults.rateUs.set(Defaults.rateUs.intValue + 1)
        }
    }
}
