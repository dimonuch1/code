//
//  ProfileModel.swift
//  CaseSimulator
//
//  Created by kira on 2/21/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit

struct Rank {
    var exp: Int!
    var image: String!
}

protocol ProfileModelProtocol {
    func amountRanks() -> Int
    func item(at indexPath: IndexPath) -> RankEnum
    func updateModel()
    func currentexp() -> Int
}

final class ProfileModel {

    private let ranksArray: [RankEnum] = [.herald, .guardian, .crusader, .archon, .legend, .ancient, .divine, .immortal]
}

extension ProfileModel: ProfileModelProtocol {
    func amountRanks() -> Int {
        return ranksArray.count
    }

    func item(at indexPath: IndexPath) -> RankEnum {
        return ranksArray[indexPath.row]
    }

    func updateModel() {

    }

    func currentexp() -> Int {
        return Defaults.experions.intValue
    }

}
