//
//  ProfileViewController.swift
//  CaseSimulator
//
//  Created by kira on 2/21/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit
import Crashlytics

final class ProfileViewController: UIViewController, InitFromStoryboard {

    typealias Model = ProfileViewController

    // MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var youExpLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var userNametextField: UITextField!
    @IBOutlet weak var yourXpLabel: UILabel!
    @IBOutlet weak var xpToRankUp: UILabel!
    @IBOutlet weak var currentRankLabel: UILabel!
    @IBOutlet weak var currentRankImageView: UIImageView!
    @IBOutlet weak var xpToNextRankYellowLabel: UILabel!

    @IBOutlet weak var ranksLabel: UILabel!
    @IBOutlet weak var ranksLabelDescriptionOne: UILabel!
    @IBOutlet weak var ranksDescriptionTwoLabel: UILabel!
    // MARK: - Properties
    private var model: ProfileModelProtocol = ProfileModel()
    private var edgeInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }

    private let amountColums: CGFloat = 3
    private let nameView = "ProfileViewController"

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateViews()
        Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
    }

    func updateViews() {
        youExpLabel.text = "\(model.currentexp())"
        userNametextField.text = Defaults.userName.string
        let currentRunk = RankEnum.rankByExp(exp: Defaults.experions.intValue)
        currentRankLabel.text = currentRunk.rawValue.uppercased()
        currentRankImageView.image = currentRunk.image
        xpToNextRankYellowLabel.text = "\(xpToNextRank())"

        if Defaults.userName.string == "" {
            userNametextField.placeholder = "user name"
        }
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }

    private func config() {
        collectionView.register(R.nib.rankCollectionViewCell)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
        localizationConfig()
        userNametextField.delegate = self
        userNametextField.returnKeyType = UIReturnKeyType.done
    }

    private func localizationConfig() {
        yourXpLabel.text = R.string.localization.profileYourEx()
        xpToRankUp.text  = R.string.localization.profileXpToRankUp()
        ranksLabel.text = R.string.localization.profileInfoTitle()
        ranksLabelDescriptionOne.text = R.string.localization.profileInfoDescription()
        ranksDescriptionTwoLabel.text  = R.string.localization.profileInfoInfo()
    }

    private func xpToNextRank() -> Int {
        let currentRank = RankEnum.rankByExp(exp: Defaults.experions.intValue)
        if currentRank != .immortal {
            let nextRank = RankEnum.rankByExp(exp: Defaults.experions.intValue).nextRank
            return nextRank.neededExp - Defaults.experions.intValue
        }
        return 0
    }
}

// MARK: - UICollectionViewDataSource
extension ProfileViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.amountRanks()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.rankCollectionViewCell.identifier,
                                                            for: indexPath) as? RankCollectionViewCell else { return UICollectionViewCell() }
        cell.config(model: model.item(at: indexPath))
        return cell
    }
}

// MARK: -
extension ProfileViewController {

}

// MARK: - UICollectionViewDelegateFlowLayout
extension ProfileViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((collectionView.frame.width - (edgeInsets.left + edgeInsets.right)) / amountColums) - 10
        let height = ((collectionView.frame.height - edgeInsets.bottom - edgeInsets.top) / amountColums)
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return edgeInsets
    }

}

// MARK: - Actions
extension ProfileViewController {

    @IBAction func userNameAction(_ sender: UITextField) {

    }
}

// MARK: - UITextFieldDelegate
extension ProfileViewController: UITextFieldDelegate {

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        saveUserName(newName: textField.text)
        return true
    }

    private func saveUserName(newName: String?) {
        Defaults.userName.set(newName ?? "")
    }
}
