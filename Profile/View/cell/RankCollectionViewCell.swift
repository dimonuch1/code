//
//  RankCollectionViewCell.swift
//  CaseSimulator
//
//  Created by kira on 2/21/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

class RankCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var rankImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func config(model: RankEnum) {
        print("config: \(model)")
        rankImage.image = model.image
        textLabel.text = "\(model.neededExp) XP"
    }

}
