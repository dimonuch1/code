//
//  GamesFlowLayout.swift
//  CaseSimulator
//
//  Created by kira on 6/6/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

final class GamesFlowLayout: UICollectionViewFlowLayout {

    override func prepare() {
        super.prepare()

        guard let cv = collectionView else { return }
        cv.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.itemSize = CGSize(width: (cv.bounds.inset(by: cv.layoutMargins).inset(by: cv.layoutMargins).size.width) / 2
            , height: cv.bounds.inset(by: cv.layoutMargins).size.height / 2)
        self.sectionInset = UIEdgeInsets(top: self.minimumLineSpacing, left: 10.0, bottom: 10.0, right: 10.0)
        self.sectionInsetReference = .fromSafeArea
        //self.scrollDirection       = .horizontal
    }

}
