//
//  JackpotViewController.swift
//  CaseSimulator
//
//  Created by kira on 5/15/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit
import Crashlytics

enum RouletState {
    case pre
    case inTime
    case post
}

protocol ChoiceGuns: class {
    func chocenGuns(newGuns: [WinnedGunModelObject])
}

final class JackpotViewController: UIViewController, InitFromStoryboard {

    typealias Model = JackpotViewController

    weak var delegate: SundriesProtocol?
    private var audioManager = AudioManager()

    private var rouletState: RouletState = .pre

    private var shadowLayerChanceView: CAShapeLayer!

    private let maxAmountItems = 50
    private let realmProvider = RealmProvider()
    private var jackpotRurity: JackpotRurity = .easy {
        didSet {
            changeModeButton.setTitle(jackpotRurity.rawValue.uppercased(), for: .normal)
        }
    }
    private var model = [WinnedGunModelObject]() {
        didSet {
            tableView.reloadData()
        }
    }

    private var userModel = [WinnedGunModelObject]()

    private var winnigGuns = [WinnedGunModelObject]()

    private var spacing: SpacingEnum {
        let spacing = SpacingEnum(rawValue: UIDevice.current.model)
        return spacing ?? .iPhone
    }

    var timer: Timer?

    private var userPricing: Int!
    private var totalPrice: Int! {
        didSet {
            chanceView.setTotalPrice(newTotalPrice: totalPrice)
        }
    }

    private let nameView    = "JackpotViewController"
    private let winJackpot  = "WinJackpot"
    private let loseJackpot = "LoseJackpot"

    @IBOutlet var tableView: UITableView!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var listItemLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!

    @IBOutlet var chanceView: ChanceView!
    @IBOutlet var startingJacpotView: UILabel!

    @IBOutlet var addItemButton: UIButton!
    @IBOutlet var addItemTexture: UIImageView!

    @IBOutlet var startRouletButton: UIButton!
    @IBOutlet var startRouletTexture: UIImageView!

    @IBOutlet var changeModeButton: UIButton!
    @IBOutlet var changeModeTexture: UIImageView!

    @IBOutlet var confirmButton: UIButton!
    @IBOutlet var confirmTexture: UIImageView!

    @IBOutlet var roundedViews: [UIView]!

    @IBOutlet var rouletPersonView: RouletPersonView!

    @IBOutlet var titleView: UIView!
    @IBOutlet var closeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutSubviews()
        Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
        if Defaults.amountOpenJackpot.intValue == 10 {
            delegate?.showAdvertisingFromCase()
            Defaults.amountOpenJackpot.set(0)
        }
        Defaults.amountOpenJackpot.set(Defaults.amountOpenJackpot.intValue + 1)
    }

    private func config() {

        tableView.register(R.nib.jackpotTableViewCell(), forCellReuseIdentifier: JackpotTableViewCell.identifire)
        tableView.dataSource = self
        tableView.delegate   = self
        amountLabel.text     = "\(model.count)/\(maxAmountItems)"

        localization()

        tableView.layer.borderWidth = 1
        tableView.layer.borderColor = UIColor.white.cgColor

        titleView.layer.borderWidth = 1
        titleView.layer.borderColor = UIColor.white.cgColor

        roundSingle(targetView: titleView)
        roundSingle(targetView: tableView)

        addItemTexture.rounded(radius: 5)
        startRouletTexture.rounded(radius: 5)
        confirmTexture.rounded(radius: 5)
        changeModeTexture.rounded(radius: 5)
    }

    private func localization() {
        listItemLabel.text   = R.string.localization.jackpotListItem()
        valueLabel.text      = R.string.localization.jackpotValue()
        startRouletButton.setTitle(R.string.localization.jackpotStart(), for: .normal)
        addItemButton.setTitle(R.string.localization.jackpotItems(), for: .normal)
        confirmButton.setTitle(R.string.localization.jackpotConfirm(), for: .normal)

    }

    override func viewDidLayoutSubviews() {
        roundedView()
        chanceView.rounded(radius: 10)
    }

    private func roundedView() {
        roundedViews.forEach { roundSingle(targetView: $0) }
        roundedViews.forEach { shdowSingle(targetView: $0) }
    }

    private func roundSingle(targetView: UIView) {
        targetView.layer.cornerRadius = 5
        targetView.clipsToBounds = true
    }

    private func shdowSingle(targetView: UIView) {

        targetView.layer.masksToBounds = false
        targetView.layer.shadowColor   = UIColor.black.cgColor
        targetView.layer.shadowOpacity = 0.5
        targetView.layer.shadowOffset  = CGSize(width: 1, height: 1)
        targetView.layer.shadowRadius  = 5

        targetView.layer.shadowPath         = UIBezierPath(rect: targetView.bounds).cgPath
        targetView.layer.shouldRasterize    = true
        targetView.layer.rasterizationScale = true ? UIScreen.main.scale : 1
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {}

}

// MARK: - Actions
extension JackpotViewController {
    @IBAction func addItemDidTap() {
        audioManager.play(sound: .click)
        delegate?.showItemChoicer(with: self)
    }

    @IBAction func rurityTap() {
        audioManager.play(sound: .click)
        changeJackpotRurity()
    }

    @IBAction func startTap() {
        if model.count == 10 {
            audioManager.play(sound: .click)
            startRoulet()
        } else {
            let alertController = UIAlertController(title: R.string.localization.jackpotAlert(),
                                                    message: nil,
                                                    preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            present(alertController, animated: true, completion: nil)
        }
    }

    @IBAction func confirmTaped() {
        audioManager.play(sound: .click)
        chanceView.setToStartState()
        rouletState = .pre
        jackpotRurity = .easy
        model.removeAll()
        userModel.removeAll()
        winnigGuns.removeAll()
        userPricing = 0
        totalPrice  = 0
        tableView.reloadData()
        confirmButton.isHidden     = true
        confirmTexture.isHidden    = true
        addItemButton.isHidden     = false
        addItemTexture.isHidden    = false
        startRouletButton.isHidden = false
        startRouletTexture.isHidden = false
        changeModeButton.isHidden  = false
        changeModeTexture.isHidden = false
    }

    @IBAction func closeButtonTaped(_ sender: Any) {
        audioManager.play(sound: .close)
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension JackpotViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: JackpotTableViewCell.identifire) as? JackpotTableViewCell else {  return UITableViewCell() }
        cell.config(model: model[indexPath.row])
        return cell
    }

}

// MARK: - UITableViewDelegate
extension JackpotViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return spacing.tableViewRowHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

// MARK: - ChoiceGuns
extension JackpotViewController: ChoiceGuns {
    func chocenGuns(newGuns: [WinnedGunModelObject]) {
        model = newGuns
        userModel = newGuns
        amountLabel.text = "\(model.count)/\(maxAmountItems)"
        userPricing = price()
        totalPrice = userPricing
        chanceView.setYourChance(chance: 100)
        chanceView.setYourItemsPrice(newYourPrice: userPricing)
    }
}

// MARK: - Helper
extension JackpotViewController {

    private func changeJackpotRurity() {
        switch jackpotRurity {
        case .easy: jackpotRurity   = .medium
        case .medium: jackpotRurity = .hard
        case .hard: jackpotRurity   = .easy
        }
    }

    private func price() -> Int {
        var sum = 0
        for item in model {
            sum += Int(item.price)
        }
        return sum
    }

    private func randomWinnedGunModelObject() -> WinnedGunModelObject {
        return WinnedGunModelObject(gun: realmProvider.randomItem(type: GunModelObject.self))
    }

    private func startRoulet() {
        changeViewVision(type: .inTime)
        tableView.isUserInteractionEnabled = false
        audioManager.play(sound: .cocking )
        timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: true, block: { [weak self] (timer) in

            if self?.model.count != self?.maxAmountItems {
                if let newItem = self?.randomWinnedGunModelObject() {
                    self?.model.append(newItem)
                    self?.winnigGuns.append(newItem)
                    self?.totalPrice += Int(newItem.price)
                    self?.chanceView.setYourChance(chance: self?.calculateWinPrecent() ?? 100)

                    self?.amountLabel.text = "\(self!.model.count)/\(self!.maxAmountItems)"
                    self?.tableView.scrollToRow(at: IndexPath(item: (self?.model.count)! - 1, section: 0),
                                                at: .bottom, animated: false)
                }
            } else {
                //конец добавления айтемов. их 50
                self?.audioManager.play(sound: .shoot)
                self?.tableView.isUserInteractionEnabled = true
                timer.invalidate()
                self?.rouletPersonView.isHidden = false
                self?.rouletPersonView.start(jackpotRurity: self?.jackpotRurity ?? .easy) { (isWin) in

                    if isWin {
                        Answers.logContentView(withName: self?.winJackpot,
                                               contentType: "isWinContentType",
                                               contentId: "isWinContentId",
                                               customAttributes: nil)
                    } else {
                        Answers.logContentView(withName: self?.loseJackpot,
                                               contentType: nil,
                                               contentId: nil,
                                               customAttributes: nil)
                    }

                    self?.changeViewVision(type: .post)
                    self?.chanceView.setWinLabel(isWin: isWin)
                    self?.manipulateGunsAfterRoulet(isWin: isWin)
                }
            }
        })
    }

    private func calculateWinPrecent() -> Int {
        return Int((CGFloat(10) / (jackpotRurity.multiple * CGFloat(10))) * CGFloat(100))
    }

    private func changeViewVision(type: RouletState) {
        switch type {
        case .pre: break
        case .inTime:
            startingJacpotView.isHidden = false
            addItemButton.isHidden      = true
            addItemTexture.isHidden     = true
            startRouletButton.isHidden  = true
            startRouletTexture.isHidden = true
            changeModeButton.isHidden   = true
            changeModeTexture.isHidden  = true
            closeButton.isHidden        = true
        case .post:
            rouletPersonView.isHidden   = true
            confirmButton.isHidden      = false
            confirmTexture.isHidden     = false
            startingJacpotView.isHidden = true
            closeButton.isHidden        = false
        }
    }

    private func manipulateGunsAfterRoulet(isWin: Bool) {
        switch isWin {
        case true: saveGunsIfWin()
        case false: deleteGunsIfLose()
        }
        App.updateValue(newValue: realmProvider.totalPriceWinnedGun())
    }
}

//MARK: - manipulate with guns after roulet
extension JackpotViewController {
    private func saveGunsIfWin() {
        for item in winnigGuns {
            realmProvider.create(object: item)
        }
    }

    private func deleteGunsIfLose() {
        for item in userModel {
            realmProvider.delete(type: WinnedGunModelObject.self, with: item.uuId)
        }
    }
}
