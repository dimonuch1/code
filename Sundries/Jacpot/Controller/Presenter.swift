//
//  Presenter.swift
//  CaseSimulator
//
//  Created by kira on 5/15/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit

func presentJackpot(in vc: UIViewController) {
    let jackpotController = R.storyboard.jackpot.instantiateInitialViewController()!
    vc.present(jackpotController, animated: true, completion: nil)
}
