//
//  LeaderboardViewController.swift
//  CaseSimulator
//
//  Created by kira on 6/29/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit
import GameKit

final class LeaderboardViewController: UIViewController {

    var gcEnabled: Bool?
    var gcDefaultLeaderBoard: String?

    var score = 0

    //let LEADERBOARD_ID = "com.homecat116.CaseSimulator.Richest"

    override func viewDidLoad() {
        super.viewDidLoad()

        authenticateLocalPlayer()

    }

    func checkGCLeaderboard(_ sender: AnyObject) {
        let gcVC = GKGameCenterViewController()
        gcVC.gameCenterDelegate = self
        gcVC.viewState = .leaderboards
        gcVC.leaderboardIdentifier = Leaderboards.richest.rawValue
        present(gcVC, animated: true, completion: nil)
    }

    // MARK: - AUTHENTICATE LOCAL PLAYER
    func authenticateLocalPlayer() {
        let localPlayer: GKLocalPlayer = GKLocalPlayer.local

        localPlayer.authenticateHandler = {(ViewController, error) -> Void in
            if let controller = ViewController {
                // 1. Show login if player is not logged in
                self.present(controller, animated: true, completion: nil)
            } else if (localPlayer.isAuthenticated) {
                // 2. Player is already authenticated & logged in, load game center
                self.gcEnabled = true

                // Get the default leaderboard ID
                localPlayer.loadDefaultLeaderboardIdentifier(completionHandler: { (leaderboardIdentifer, error) in
                    if error != nil { print(error)
                    } else { self.gcDefaultLeaderBoard = leaderboardIdentifer! }
                })

            } else {
                // 3. Game center is not enabled on the users device
                self.gcEnabled = false
                print("Local player could not be authenticated!")
                print(error)
            }
        }
    }

}

extension LeaderboardViewController: GKGameCenterControllerDelegate {

    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {

    }

}
