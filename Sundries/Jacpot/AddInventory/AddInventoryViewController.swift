//
//  AddInventoryViewController.swift
//  CaseSimulator
//
//  Created by kira on 5/15/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

protocol DidChoiceItemProtocol: class {
    func didSelect(item: WinnedGunModelObject)
}

final class AddInventoryViewController: UIViewController {

    @IBOutlet var itemsCollectionView: UICollectionView!
    @IBOutlet var choicedCollectionView: ChoicedUICollectionView!
    @IBOutlet var titleLable: UILabel!
    @IBOutlet var itemsCostLabel: UILabel!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var choseButton: UIButton!

    private var audioManager = AudioManager()

    private let realmProvider = RealmProvider()
    private var model = [WinnedGunModelObject]() {
        didSet {
            itemsCollectionView.reloadData()
        }
    }

    weak var addNewItemDelegate: DidChoiceItemProtocol?

    weak var deleagate: ChoiceGuns?

    private var spacing: SpacingEnum {
        let spacing = SpacingEnum(rawValue: UIDevice.current.model)
         return spacing ?? .iPhone
    }

    private var columsItemCollectionView: CGFloat = 3

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }

    private func config() {
        itemsCollectionView.register(R.nib.inventoryCollectionViewCell)
        choicedCollectionView.register(R.nib.choicedCollectionViewCell)

        if let newModel = realmProvider.fetchAllByCase(with: nil, type: WinnedGunModelObject.self) as? [WinnedGunModelObject] {
            model = newModel
        }

        itemsCollectionView.delegate = self
        itemsCollectionView.dataSource = self

        choicedCollectionView.dataSource = choicedCollectionView
        choicedCollectionView.delegate = choicedCollectionView

        itemsCollectionView.reloadData()
        addNewItemDelegate = choicedCollectionView
        choicedCollectionView.deselectDelegate = self

        itemsCollectionView.flowLayout?.minimumInteritemSpacing = spacing.internalSpacing
        itemsCollectionView.flowLayout?.minimumLineSpacing = spacing.lineSpacing

        choicedCollectionView.flowLayout?.minimumInteritemSpacing = spacing.internalSpacing
        choicedCollectionView.flowLayout?.minimumLineSpacing = spacing.lineSpacing

        changeTotalCost(totalCost: 0)

        itemsCollectionView.layer.borderColor = UIColor.white.cgColor
        itemsCollectionView.layer.borderWidth = 1

        choicedCollectionView.layer.borderColor = UIColor.white.cgColor
        choicedCollectionView.layer.borderWidth = 1

        roundSingle(targetView: itemsCollectionView)
        roundSingle(targetView: choicedCollectionView)

        roundSingle(targetView: closeButton)
        roundSingle(targetView: choseButton)

        shdowSingle(targetView: closeButton)
        shdowSingle(targetView: choseButton)

        closeButton.setTitle(R.string.localization.jackpotAddClose(), for: .normal)
        choseButton.setTitle(R.string.localization.jackpotAddChoose(), for: .normal)
        titleLable.text = R.string.localization.jackpotAddItemsTtile().capitalized

    }

    private func roundSingle(targetView: UIView) {
        targetView.layer.cornerRadius = 5
        targetView.clipsToBounds = true
    }

    private func shdowSingle(targetView: UIView) {

        targetView.layer.masksToBounds = false
        targetView.layer.shadowColor   = UIColor.black.cgColor
        targetView.layer.shadowOpacity = 0.5
        targetView.layer.shadowOffset  = CGSize(width: 1, height: 1)
        targetView.layer.shadowRadius  = 5

        targetView.layer.shadowPath         = UIBezierPath(rect: targetView.bounds).cgPath
        targetView.layer.shouldRasterize    = true
        targetView.layer.rasterizationScale = true ? UIScreen.main.scale : 1
    }


    @IBAction func close() {
        audioManager.play(sound: .close)
        dismiss(animated: true, completion: nil)
    }

    @IBAction func  confirm() {
        audioManager.play(sound: .click)
        deleagate?.chocenGuns(newGuns: choicedCollectionView.getAllChoicedItems())
        dismiss(animated: true, completion: nil)
    }

}

// MARK: - UICollectionViewDataSource
extension AddInventoryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.inventoryCollectionViewCell.identifier, for: indexPath) as? InventoryCollectionViewCell else {
            return UICollectionViewCell()
        }

        cell.config(model: model[indexPath.row])
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension AddInventoryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if choicedCollectionView.isHaveSpace() {
            addNewItemDelegate?.didSelect(item: model[indexPath.row])
            model.remove(at: indexPath.row)
            itemsCollectionView.reloadData()
        }
    }
}

// MARK: DidDeselectChoicedItem
extension AddInventoryViewController: DidDeselectChoicedItem {
    func didDeselect(item: WinnedGunModelObject) {
        model.append(item)
    }

    func changeTotalCost(totalCost: Int) {
        itemsCostLabel.text = String(Int(totalCost)) + "$"
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension AddInventoryViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (itemsCollectionView.bounds.width / columsItemCollectionView) - spacing.internalSpacing
        let height = (itemsCollectionView.bounds.height / columsItemCollectionView) - spacing.lineSpacing
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}
