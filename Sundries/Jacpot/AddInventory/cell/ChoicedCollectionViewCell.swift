//
//  ChoicedCollectionViewCell.swift
//  CaseSimulator
//
//  Created by kira on 6/3/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

final class ChoicedCollectionViewCell: UICollectionViewCell {

    @IBOutlet var image: UIImageView!
    @IBOutlet var price: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func config(model: WinnedGunModelObject) {
        image.image = UIImage(named: model.image)
        price.text = "\(Int(model.price))$"
    }

}
