//
//  ChoicedUICollectionView.swift
//  CaseSimulator
//
//  Created by kira on 5/16/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

protocol DidDeselectChoicedItem: class {
    func didDeselect(item: WinnedGunModelObject)
    func changeTotalCost(totalCost: Int)
}

final class ChoicedUICollectionView: UICollectionView {

    weak var deselectDelegate: DidDeselectChoicedItem?

    private var columsItemCollectionView: CGFloat = 5

    private var spacing: SpacingEnum {
        let spacing = SpacingEnum(rawValue: UIDevice.current.model)
        return spacing ?? .iPhone
    }

    private var model = [WinnedGunModelObject]() {
        didSet {
            updateCost()
            reloadData()
        }
    }

    public func getAllChoicedItems() -> [WinnedGunModelObject] {
        return model
    }

    public func isHaveSpace() -> Bool {
        return model.count < 10
    }

    private func updateCost() {
        var totalCost: CGFloat = 0
        for item in model {
            totalCost += item.price
        }
        deselectDelegate?.changeTotalCost(totalCost: Int(totalCost))
    }
    
}

// MARK: - UICollectionViewDataSource
extension ChoicedUICollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.choicedCollectionViewCell.identifier,
                                                            for: indexPath) as? ChoicedCollectionViewCell else { return UICollectionViewCell() }
        cell.config(model: model[indexPath.row])
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension ChoicedUICollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        deselectDelegate?.didDeselect(item: model[indexPath.row])
        model.remove(at: indexPath.row)
        reloadData()
    }
}

// MARK: - DidChoiceItemProtocol
extension ChoicedUICollectionView: DidChoiceItemProtocol {
    func didSelect(item: WinnedGunModelObject) {
        model.append(item)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ChoicedUICollectionView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width  = ((self.bounds.width - 20) / columsItemCollectionView) - spacing.internalSpacing
        let height = (self.bounds.height / 2) - spacing.lineSpacing

        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    }
}
