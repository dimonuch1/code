//
//  ChanceView.swift
//  CaseSimulator
//
//  Created by kira on 5/25/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

protocol ChanceViewProtocol: class {
    func setTotalPrice(newTotalPrice: Int)
    func setYourItemsPrice(newYourPrice: Int)
    func setYourChance(chance: Int)
    func setToStartState()
}

final class ChanceView: UIView {

    @IBOutlet var view: UIView!

    private var shadowLayerChanceView: CAShapeLayer!

    //OUTlets pre
    private var yourItemsPrice  = 0
    private var totalItemsPrice = 0

    @IBOutlet var vibrancBackground: UIView!
    @IBOutlet var backgroundImage: UIImageView!

    @IBOutlet var totalPriceLabel: UILabel!
    @IBOutlet var titleTotalChanceLabel: UILabel!

    @IBOutlet var yourPriceLabel: UILabel!
    @IBOutlet var titleYourPriceLabel: UILabel!

    @IBOutlet var chanceLabel: UILabel!
    @IBOutlet var titleChanceLabel: UILabel!

    //OUTlets post
    @IBOutlet var winTypeLabel: UILabel!

    private let defaultVibranceColor: UIColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)

    private var viewType: ChanceViewType = .pre
    private var winType: ChanceSate? {
        didSet {
            winTypeLabel.isHidden     = false
            chanceLabel.isHidden      = true
            titleChanceLabel.isHidden = true
            vibrancBackground.backgroundColor = winType?.backgroundColor
            winTypeLabel.text = winType?.titleForLabel.capitalized
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commomInit()
    }

    private func commomInit() {
        print("NIB NAME: \(R.nib.chanceView.name)")
        Bundle.main.loadNibNamed(R.nib.chanceView.name, owner: self, options: nil)
        addSubviewWithDefaultConstraints(view)

        titleChanceLabel.text = R.string.localization.jackpotChance().uppercased()
        titleTotalChanceLabel.text = R.string.localization.jackpotChanceViewTotal().uppercased()
        titleYourPriceLabel.text = R.string.localization.jackpotChanceViewYour().uppercased()

    }

}

//pre-post state
// MARK: - ChanceViewProtocol
extension ChanceView: ChanceViewProtocol {

    func setTotalPrice(newTotalPrice: Int) {
        totalPriceLabel.text = "\(newTotalPrice)$"
    }

    func setYourItemsPrice(newYourPrice: Int) {
        yourPriceLabel.text = "\(newYourPrice)$"
    }

    func setYourChance(chance: Int) {
        chanceLabel.text = "\(Int(chance))" + "%"
    }

    func setWinLabel(isWin: Bool) {
        switch isWin {
        case true: winType  = .win
        case false: winType = .lose
        }
    }

    func setToStartState() {
        viewType = .pre
        vibrancBackground.backgroundColor = defaultVibranceColor
        winTypeLabel.isHidden     = true
        chanceLabel.isHidden      = false
        titleChanceLabel.isHidden = false
        totalPriceLabel.text      = "0$"
        yourPriceLabel.text       = "0$"
        chanceLabel.text          = "100%"
    }
}
