//
//  RouletPersonView.swift
//  CaseSimulator
//
//  Created by kira on 6/1/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

final class RouletPersonView: UIView {

    @IBOutlet var view: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var strokeLabel: UILabel!

    private var jackpotRurity: JackpotRurity = .easy

    private let userViewsAmount = 10

    private var arrayPlayers: [PersonView] = [PersonView]()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commomInit()

    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        strokeLabel.layer.cornerRadius = 5
        strokeLabel.clipsToBounds = true
    }

    private func commomInit() {
        Bundle.main.loadNibNamed("RouletPersonView", owner: self, options: nil)
        addSubviewWithDefaultConstraints(view)
    }

    public func start(jackpotRurity: JackpotRurity, completion: @escaping(_ isWin: Bool) -> Void) {
        self.jackpotRurity = jackpotRurity
        fillScrollView()
        animate { (isWin) in
            completion(isWin)
        }
    }

    func animate(completion: @escaping(_ isWin: Bool) -> Void) {

        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 7, delay: 0, options: .curveEaseInOut, animations: {
            self.scrollView.contentOffset.x = self.estimateWidth() - self.scrollView.bounds.size.width - self.scrollView.bounds.size.width / 4
        }) { (position) in
            completion(self.computedWinner())
        }
    }

    private func computedWinner() -> Bool {
        for view in scrollView.subviews {
            let rectOfViewnSuperview = self.scrollView.convert(view.frame, to: self.view)
            if rectOfViewnSuperview.intersects(self.strokeLabel.frame) {
                if let view = view as? PersonView {
                    return view.personType.isWin
                }
            }
        }
        return false
    }
}

// MARK: - Init DATA
extension RouletPersonView {

    private func clearAll() {
        arrayPlayers.removeAll()
        for item in scrollView.subviews {
            item.removeFromSuperview()
        }
        self.scrollView.contentOffset.x = 0
    }

    private func createArrayPlayers() {
        clearAll()

        //create me
        for _ in 0...userViewsAmount {
            let newView = PersonView()
            newView.config(image: UIImage(named: "ct") ?? UIImage(), name: "You", personType: .me)
            arrayPlayers.append(newView)
        }

        //create enemy
        for _ in 0...Int(CGFloat(userViewsAmount) * jackpotRurity.multiple) {
            let newView = PersonView()
            newView.config(image: UIImage(named: "t") ?? UIImage(), name: "Enemy", personType: .enemy)
            arrayPlayers.append(newView)
        }

        //shuffle
        arrayPlayers = arrayPlayers.shuffled()
    }

    private func fillScrollView() {

        createArrayPlayers()

        guard let firstView = arrayPlayers.first else { return }
        setFirstView(firstView: firstView)

        for (index, item) in arrayPlayers.enumerated() {
            if index >= 1 {
                fillNewView(newView: item, preView: arrayPlayers[index - 1])
            }
        }
    }

    private func estimateWidth() -> CGFloat {
        let width = scrollView.bounds.width
        return CGFloat(arrayPlayers.count) * (CGFloat(width) / CGFloat(3))
    }

    private func setFirstView(firstView: UIView) {
        scrollView.addSubview(firstView)
        firstView.snp.makeConstraints { (make) in
            make.height.equalTo(scrollView)
            make.width.equalTo(scrollView.bounds.width / 3)
            make.left.equalTo(0)
            make.centerY.equalTo(scrollView)
        }
    }

    private func fillNewView(newView: UIView, preView: UIView) {
        scrollView.addSubview(newView)
        newView.snp.makeConstraints { (make) in
            make.height.equalTo(preView)
            make.width.equalTo(preView)
            make.centerY.equalTo(preView)
            make.left.equalTo(preView.snp.right)
        }
    }
}
