//
//  PersonView.swift
//  CaseSimulator
//
//  Created by kira on 6/1/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

enum PersonType {
    case me
    case enemy

    var labelColor: UIColor {
        switch self {
        case .me: return .green
        case .enemy: return .red
        }
    }

    var isWin: Bool {
        switch self {
        case .me: return true
        case .enemy: return false
        }
    }
}

final class PersonView: UIView {

    @IBOutlet var view: UIView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    var personType: PersonType = .enemy

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("PersonView", owner: self, options: nil)
        addSubviewWithDefaultConstraints(view)
    }

    func config(image: UIImage, name: String, personType: PersonType) {
        nameLabel.text = name
        nameLabel.backgroundColor = personType.labelColor
        self.personType = personType
        backgroundImage.image = image
        backgroundColor = .clear
    }
}
