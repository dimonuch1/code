//
//  GamesCollectionViewCell.swift
//  CaseSimulator
//
//  Created by kira on 6/6/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

//extension UICollectionViewCell {
//    static let identifire = String(describing: type(of: self))
//}

final class GamesCollectionViewCell: UICollectionViewCell {

    static let identifire = String(describing: type(of: self))

    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var comingsonnImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func config(model: GamesStruct) {
        backgroundImage.image = model.image
        titleLabel.text       = model.title
//        switch model.isComingsoon {
//        case true:
//            comingsonnImage.image = UIImage(named: "comingsoon")
//            comingsonnImage.isHidden = false
//        case false:
//            comingsonnImage.isHidden = true
//        }
    }

}
