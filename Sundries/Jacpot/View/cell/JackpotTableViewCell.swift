//
//  JackpotTableViewCell.swift
//  CaseSimulator
//
//  Created by kira on 5/15/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static let identifire: String = String(describing: type(of: self))
}

final class JackpotTableViewCell: UITableViewCell {

    @IBOutlet var rareLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func config(model: WinnedGunModelObject) {
        rareLabel.backgroundColor = model.rurityEnum.color
        nameLabel.text = model.nameCommon
        priceLabel.text = "\(Int(model.price))" + "$"
    }
    
}
