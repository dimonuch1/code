//
//  StatsModel.swift
//  CaseSimulator
//
//  Created by kira on 2/25/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

protocol StatsModelProtocol {
    func openCases() -> Int
    func gained() -> Int

    func rare() -> Int
    func mythical() -> Int
    func legendary() -> Int
    func immortal() -> Int
    func arcana() -> Int
}

final class StatsModel {}

extension StatsModel: StatsModelProtocol {

    func openCases() -> Int {
        return Defaults.amountOpenCases.intValue
    }

    func gained() -> Int {
        return Int(Defaults.gained.floatValue)
    }

    func rare() -> Int {
        return Defaults.rare.intValue
    }

    func mythical() -> Int {
        return Defaults.mythical.intValue
    }

    func legendary() -> Int {
        return Defaults.legendary.intValue
    }

    func immortal() -> Int {
        return Defaults.immortal.intValue
    }

    func arcana() -> Int {
        return Defaults.arcana.intValue
    }

}
