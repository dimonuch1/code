//
//  StatsViewController.swift
//  CaseSimulator
//
//  Created by kira on 2/25/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit
import Crashlytics

final class StatsViewController: UIViewController, InitFromStoryboard {
    typealias Model = StatsViewController

    //consumer
    @IBOutlet weak var consumerBackView: UIView!
    @IBOutlet weak var consumerColorView: UIView!
    @IBOutlet weak var consumerNameLabel: UILabel!
    @IBOutlet weak var consumerAmountLabel: UILabel!

    //industrial
    @IBOutlet weak var industrialBackView: UIView!
    @IBOutlet weak var industrialColorView: UIView!
    @IBOutlet weak var industrialNameLabel: UILabel!
    @IBOutlet weak var industrialAmountLabel: UILabel!

    //mil
    @IBOutlet weak var milBackView: UIView!
    @IBOutlet weak var milColorView: UIView!
    @IBOutlet weak var milNameLabel: UILabel!
    @IBOutlet weak var milAmountLabel: UILabel!

    //restricted
    @IBOutlet weak var restrictedBackView: UIView!
    @IBOutlet weak var restrictedColorView: UIView!
    @IBOutlet weak var restrictedrNameLabel: UILabel!
    @IBOutlet weak var restrictedAmountLabel: UILabel!

    //classifield
    @IBOutlet weak var classifieldBackView: UIView!
    @IBOutlet weak var classifieldColorView: UIView!
    @IBOutlet weak var classifieldNameLabel: UILabel!
    @IBOutlet weak var classifieldAmountLabel: UILabel!

    //covert
    @IBOutlet weak var covertBackView: UIView!
    @IBOutlet weak var covertColorView: UIView!
    @IBOutlet weak var covertNameLabel: UILabel!
    @IBOutlet weak var covertAmountLabel: UILabel!

    //contraband
    @IBOutlet weak var contrabandBackView: UIView!
    @IBOutlet weak var contrabandColorView: UIView!
    @IBOutlet weak var contrabandNameLabel: UILabel!
    @IBOutlet weak var contrabandAmountLabel: UILabel!

    //title
    @IBOutlet weak var titleView: UIView!

    @IBOutlet weak var caseTitle: UILabel!
    @IBOutlet weak var caseAmount: UILabel!

    @IBOutlet weak var gainedTitle: UILabel!
    @IBOutlet weak var gainedAmount: UILabel!

    //weapons
    @IBOutlet weak var weaponsTitle: UILabel!
    @IBOutlet weak var weaponView: UIView!

    private var model: StatsModelProtocol = StatsModel()
    private let nameView = "StatsViewController"

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        DispatchQueue.main.async {
            self.config()
        }

        configFromModel()
        Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
    }

    private func config() {
//weapon
        titleView.rounded()
        titleView.dropShadow()

        weaponsTitle.attributedText = NSAttributedString(string: R.string.localization.statsWeapons().uppercased())
        weaponView.rounded()
        weaponView.dropShadow()


        caseTitle.text = R.string.localization.statsCases().capitalized
        caseAmount.text = String(model.openCases()).uppercased()
        gainedTitle.text = R.string.localization.statsGained().capitalized


//consumer
        consumerBackView.dropShadow()
        //consumerColorView.backgroundColor = RuritySkin.Consumer_Grade.color
        consumerNameLabel.text = "consumer".uppercased()
        consumerBackView.rounded(radius: 3)

//industrial
        industrialBackView.dropShadow()
        //industrialColorView.backgroundColor = RuritySkin.Industrial_Grade.color
        industrialNameLabel.text = "industrial".uppercased()
        industrialBackView.rounded(radius: 3)

//mil
        milBackView.dropShadow()
       // milColorView.backgroundColor = RuritySkin.Mil_Spec.color
        //milNameLabel.text = RuritySkin.Mil_Spec.rawValue.uppercased()
        milBackView.rounded(radius: 3)

//restricted
        restrictedBackView.dropShadow()
        //restrictedColorView.backgroundColor = RuritySkin.Restricted.color
        //restrictedrNameLabel.text = RuritySkin.Restricted.rawValue.uppercased()
        restrictedBackView.rounded(radius: 3)

//classifield
        classifieldBackView.dropShadow()
        //classifieldColorView.backgroundColor = RuritySkin.Classifield.color
        //classifieldNameLabel.text = RuritySkin.Classifield.rawValue.uppercased()
        classifieldBackView.rounded(radius: 3)

//covert
        covertBackView.dropShadow()
        //covertColorView.backgroundColor = RuritySkin.Covert.color
        //covertNameLabel.text = RuritySkin.Covert.rawValue.uppercased()
        covertBackView.rounded(radius: 3)

//contraband
        contrabandBackView.dropShadow()
        //contrabandColorView.backgroundColor = RuritySkin.Contraband.color
        //contrabandNameLabel.text = RuritySkin.Contraband.rawValue.uppercased()
        contrabandBackView.rounded(radius: 3)
    }

    private func configFromModel() {
        gainedAmount.text = "\(model.gained())" + "$"
        consumerAmountLabel.text = "test"//"\(model.consumer())"
        industrialAmountLabel.text = "test"//"\(model.industrial())"
        milAmountLabel.text = "test"//"\(model.mil())"
        restrictedAmountLabel.text = "test"//"\(model.restricted())"
        classifieldAmountLabel.text = "test"//"\(model.classified())"
        covertAmountLabel.text = "test"//"\(model.covert())"
        contrabandAmountLabel.text = "test"//"\(model.contraband())"
    }

    @IBAction func close() {
        dismiss(animated: true, completion: nil)
    }
}
