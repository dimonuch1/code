//
//  GamesViewController.swift
//  CaseSimulator
//
//  Created by kira on 6/3/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit
import Crashlytics
import GameKit


enum DestinationGames {
    case leaderboard
    case jackpot
    case stats
}

struct GamesStruct {
    var image: UIImage
    var title: String
    var isComingsoon: Bool
    var destination: DestinationGames
}

final class GamesViewController: UIViewController, InitFromStoryboard {

    typealias Model = GamesViewController
    private let nameView = "GamesViewController"

    weak var delegate: CustomtabBarControllerAdvertising?
    private var audioManager = AudioManager()

    @IBOutlet var collectionView: UICollectionView!
    var model                    = [GamesStruct]()
    private let arrayImages      = [UIImage(named: "ct_1"), UIImage(named: "launchImage2")]
    private let attayTitles      = [R.string.localization.gamesLeaderboard().uppercased(),
                                    R.string.localization.gamesJackpot().uppercased()]
    private let arrayComingsoon  = [true, false]
    private let arrayDestination: [DestinationGames] = [.leaderboard, .jackpot]

    var gcEnabled: Bool?
    var gcDefaultLeaderBoard: String?

    var score = 0

    private let nameViewLeaderboard = "Leaderboard"

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        test()
    }

    func test() {
        let localPlayer: GKLocalPlayer = GKLocalPlayer.local

        if localPlayer.isAuthenticated, let identifire = App.gcDefaultLeaderBoard  {
            let gkScore = GKScore(leaderboardIdentifier: identifire)
            print(gkScore.context)
            print(gkScore.date)
            print(gkScore.formattedValue)
            print(gkScore.player)
            print(gkScore.rank)
            print(gkScore.value)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
    }

    private func config() {
        collectionView.delegate = self
        collectionView.dataSource = self

        createModel()

        collectionView.register(UINib(nibName: "GamesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GamesCollectionViewCell")

        collectionView.isScrollEnabled = true
        if let layout = collectionView?.collectionViewLayout as? AnimatedCollectionViewLayout {
            layout.scrollDirection = .horizontal
            layout.animator = LinearCardAttributesAnimator()
        }
        
    }

    func createModel() {
        if !arrayImages.isEmpty, !attayTitles.isEmpty, !arrayComingsoon.isEmpty, !arrayDestination.isEmpty {
            for index in 0...arrayImages.count - 1 {
                let item = GamesStruct(image: arrayImages[index]!, title: attayTitles[index], isComingsoon: arrayComingsoon[index], destination: arrayDestination[index])
                model.append(item)
            }
        }
    }

    func showLeaderboard() {
        if let identifire = App.gcDefaultLeaderBoard {
            let gcVC = GKGameCenterViewController()
            gcVC.gameCenterDelegate = self
            gcVC.viewState = .leaderboards
            gcVC.leaderboardIdentifier = identifire
            present(gcVC, animated: true, completion: nil)
        }
    }

    // MARK: - AUTHENTICATE LOCAL PLAYER
    func authenticateLocalPlayer() {
        let localPlayer: GKLocalPlayer = GKLocalPlayer.local

        localPlayer.authenticateHandler = { [weak self] (ViewController, error) -> Void in
            if let controller = ViewController {
                // 1. Show login if player is not logged in
                self?.present(controller, animated: true, completion: nil)
            } else if (localPlayer.isAuthenticated) {
                // 2. Player is already authenticated & logged in, load game center
                self?.gcEnabled = true

                // Get the default leaderboard ID
                localPlayer.loadDefaultLeaderboardIdentifier(completionHandler: { (leaderboardIdentifer, error) in
                    if error != nil { print(error)
                    } else {
                        self?.gcDefaultLeaderBoard = leaderboardIdentifer!

                        let gcVC = GKGameCenterViewController()
                        gcVC.gameCenterDelegate = self
                        gcVC.viewState = .leaderboards
                        gcVC.leaderboardIdentifier = Leaderboards.richest.rawValue
                        self?.present(gcVC, animated: true, completion: nil)
                    }
                })

            } else {
                // 3. Game center is not enabled on the users device
                self?.gcEnabled = false
                print("Local player could not be authenticated!")
                print(error)
            }
        }
    }

}

// MARK: - UICollectionViewDataSource
extension GamesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GamesCollectionViewCell", for: indexPath) as? GamesCollectionViewCell else {
            return UICollectionViewCell()
        }

        cell.config(model: model[indexPath.row])

        cell.contentView.rounded(radius: 40)

        return cell
    }

}

// MARK: - UICollectionViewDelegateFlowLayout
extension GamesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width / CGFloat(1.1), height: view.bounds.height / CGFloat(1))
    }
}

// MARK: - UICollectionViewDelegate
extension GamesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch model[indexPath.row].destination {
        case .leaderboard:
            audioManager.play(sound: .click)
            showLeaderboard()
            Answers.logContentView(withName: nameViewLeaderboard, contentType: nil, contentId: nil, customAttributes: nil)
        case .jackpot:
            audioManager.play(sound: .click)
            Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
            //if Defaults.experions.intValue >= RankEnum.crusader.neededExp {
                delegate?.presentJackpot()
//            } else {
//                let alertController = UIAlertController(title: R.string.localization.jackpotAlertLvl(),
//                                                        message: nil,
//                                                        preferredStyle: .alert)
//                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
//                alertController.addAction(action)
//                present(alertController, animated: true, completion: nil)
//            }
        case .stats:
            audioManager.play(sound: .click)
            delegate?.presentStats()
        }
    }
}

// MARK: - GKGameCenterControllerDelegate
extension GamesViewController: GKGameCenterControllerDelegate {

    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }

}
