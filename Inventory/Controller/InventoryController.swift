//
//  InventoryController.swift
//  CaseSimulator
//
//  Created by kira on 2/20/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit
import Crashlytics

protocol InventoryControllerProtocol: class {
    func model(didUpdate model: InventoryModel)
}

final class InventoryController: UIViewController, InitFromStoryboard {

    typealias Model = InventoryController

    // MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var amountItems: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var amountBackImage: UIImageView!
    @IBOutlet weak var totalPriceBackImage: UIImageView!
    private let audioManager = AudioManager()

    // MARK: - Properties
    private var model: InventoryModelProtocol = InventoryModel()
    private var edgeInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    private let amountColums: CGFloat = 3
    private let nameView = "InventoryController"

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        model.updateModel()
        updateViews()
        Answers.logContentView(withName: nameView, contentType: nil, contentId: nil, customAttributes: nil)
    }

    private func config() {
        collectionView.register(R.nib.inventoryCollectionViewCell)
        model.delegate = self
        collectionView.dataSource = self
        collectionView.delegate   = self
        collectionView.reloadData()
    }

    private func updateViews() {
        amountItems.text = "\(model.amountElements()) items"
        totalPrice.text  =  String(format: "%.0f$", (model.totalPrice()))
            //+ CGFloat(Defaults.priceSoldWeapons.intValue)))

        amountItems.dropShadow()
        totalPrice.dropShadow()
        amountBackImage.rounded()
        totalPriceBackImage.rounded()
    }

    private func animateCells() {
        let scaleAnimation = AnimationType.zoom(scale: 2)
        self.collectionView.animateAll(withType: [scaleAnimation])
    }
}

// MARK: - UICollectionViewDataSource
extension InventoryController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.amountElements()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.inventoryCollectionViewCell.identifier, for: indexPath) as? InventoryCollectionViewCell else { return UICollectionViewCell() }


        cell.config(model: model.item(at: indexPath))
        cell.layer.cornerRadius = 5
        return cell
    }

}

// MARK: - UICollectionViewDelegate
extension InventoryController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showItem(indexPath: indexPath)
    }

    private func showItem(indexPath: IndexPath) {
        audioManager.play(sound: .click)
        let itemView = ItemView()
        itemView.config(model: model.item(at: indexPath), reloadMethod: reloadAfterDelete)
        view.addSubviewWithDefaultConstraints(itemView)
    }

    private func reloadAfterDelete() {
        model.updateModel()
        amountItems.text = "\(model.amountElements()) items"
        totalPrice.text  =  String(format: "%.0f$", (model.totalPrice()))
            //+ CGFloat(Defaults.priceSoldWeapons.intValue)))
        audioManager.play(sound: .sold)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension InventoryController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width  = ((collectionView.frame.width - (edgeInsets.left + edgeInsets.right)) / amountColums) - 10
        let height = ((collectionView.frame.height - edgeInsets.bottom - edgeInsets.top) / amountColums) - 20
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return edgeInsets
    }

}

// MARK: - InventoryControllerProtocol
extension InventoryController: InventoryControllerProtocol{
    func model(didUpdate model: InventoryModel) {
        collectionView.reloadData()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            self.animateCells()
        }

    }
}
