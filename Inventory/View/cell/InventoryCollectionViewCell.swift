//
//  InventoryCollectionViewCell.swift
//  CaseSimulator
//
//  Created by kira on 2/20/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

final class InventoryCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOUtlet
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var textureImageView: UIImageView!
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBotConstraint: NSLayoutConstraint!

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        priceLabel.text = ""
        nameLabel.text  = ""
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func config(model: WinnedGunModelObject, isForumStyle: Bool = false) {
        //bottomView.backgroundColor = model.rurityEnum.color
        imageView.image = UIImage(named: model.image)
        priceLabel.text =  String(format: "%.0f$", model.price)
        priceLabel.rounded(radius: 5)

        textureImageView.image = model.rurityEnum.texture

        if App.languageString == "ru" {
            nameLabel.text = model.name_rus
        } else {
            nameLabel.text = model.name
        }

        if isForumStyle {
//            bottomHeightConstraint = NSLayoutConstraint(item: bottomHeightConstraint.firstItem, attribute: bottomHeightConstraint.firstAttribute, relatedBy: bottomHeightConstraint.relation, toItem: bottomHeightConstraint.secondItem, attribute: bottomHeightConstraint.secondAttribute, multiplier: 0, constant: bottomHeightConstraint.constant)

            //bottomHeightConstraint.constant = 0
            bottomBotConstraint.constant = -bottomView.frame.height
            layoutIfNeeded()
        }

    }

}
