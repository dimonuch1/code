//
//  ItemView.swift
//  CaseSimulator
//
//  Created by kira on 3/4/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

final class ItemView: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var caseOwnerName: UILabel!
    @IBOutlet weak var imageGun: UIImageView!
    @IBOutlet weak var nameGun: UILabel!
    @IBOutlet weak var priceTitleLabel: UILabel!
    @IBOutlet weak var priceDescriptionLabel: UILabel!
    @IBOutlet weak var topBackView: UIView!
    @IBOutlet weak var bottomBackView: UIView!
    @IBOutlet var sellButton: UIButton!
    @IBOutlet weak var textureImageView: UIImageView!

    private var realmProvider = RealmProvider()
    private var primaryKey: String?
    private var price: Int?
    private let audioManager = AudioManager()

    private var afterSellMethod: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commomInit()
    }

    private func commomInit() {
        Bundle.main.loadNibNamed(R.nib.itemView.name, owner: self, options: nil)
        addSubviewWithDefaultConstraints(view)
    }

    func config(model: WinnedGunModelObject, reloadMethod: @escaping () -> Void) {

        primaryKey      = model.uuId
        price           = Int(model.price)
        afterSellMethod = reloadMethod
        textureImageView.image = model.rurityEnum.texture

        sellButton.layer.cornerRadius = sellButton.bounds.height / 2
        sellButton.clipsToBounds      = true

        imageGun.image = UIImage(named: model.image)

        if App.languageString == "ru" {
            nameGun.text       = model.name_rus
            caseOwnerName.text = model.ownerCaseNameRus
            sellButton.setTitle("Продать", for: .normal)
        } else {
            nameGun.text       = model.name
            caseOwnerName.text = model.ownerCaseName
            sellButton.setTitle("Sell", for: .normal)
        }

        priceTitleLabel.text           = R.string.localization.price() + ":"
        priceDescriptionLabel.text     = String(format: "%.0f$", model.price)
        //topBackView.backgroundColor    = model.rurityEnum.color
        //bottomBackView.backgroundColor = model.rurityEnum.color
    }

    func configFromCases(model: GunModelObject) {
        sellButton.isHidden = true
        if App.languageString == "ru" {
            nameGun.text       = model.name_rus
            caseOwnerName.text = model.ownerCase?.name_rus
        } else {
            nameGun.text       = model.name
            caseOwnerName.text = model.ownerCase?.name
        }

        imageGun.image                 = UIImage(named: model.image)
        priceTitleLabel.text           = R.string.localization.price() + ":"
        priceDescriptionLabel.text     = String(format: "%.0f$", model.price)
        topBackView.backgroundColor    = model.rurityEnum.color
        bottomBackView.backgroundColor = model.rurityEnum.color
    }

    @IBAction func closeDidTap() {
        audioManager.play(sound: .close)
        removeFromSuperview()
    }

    @IBAction func sellItem() {
        guard let primaryKey = primaryKey else { return }
        realmProvider.delete(type: WinnedGunModelObject.self, with: primaryKey)

        //save price

        Defaults.priceSoldWeapons.set(Defaults.priceSoldWeapons.intValue + (self.price ?? 0))

        guard let afterSellMethod = afterSellMethod else { return }
        afterSellMethod()

        UIView.animate(withDuration: 1, animations: {
            self.alpha = 0
        }, completion: { (result) in
            self.removeFromSuperview()
        })
    }

}
