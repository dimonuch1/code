//
//  InventoryModel.swift
//  CaseSimulator
//
//  Created by kira on 2/20/19.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import UIKit

protocol InventoryModelProtocol: class {
    func amountElements() -> Int
    func item(at indexPath: IndexPath) -> WinnedGunModelObject
    func updateModel()
    func totalPrice() -> CGFloat
    var delegate: InventoryControllerProtocol? { get set }
}

final class InventoryModel {
    weak var delegate: InventoryControllerProtocol?
    private var realmProvider = RealmProvider()
    private var model: [WinnedGunModelObject] = []
}

extension InventoryModel {

    func updateModel() {
        model.removeAll()
        model = realmProvider.fetchAllByCase(with: nil, type: WinnedGunModelObject.self) as! [WinnedGunModelObject]
        delegate?.model(didUpdate: self)
    }
}

// MARK: - InventoryModelProtocol
extension InventoryModel: InventoryModelProtocol {
    func amountElements() -> Int {
        return model.count
    }

    func item(at indexPath: IndexPath) -> WinnedGunModelObject {
        return model[indexPath.row]
    }

    func totalPrice() -> CGFloat {
        var totalPrice: CGFloat = 0
        model.forEach({ totalPrice += $0.price })
        return totalPrice
    }
}
